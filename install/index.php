<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use GD\LostCart\LostCartTable;

IncludeModuleLangFile(__FILE__);

class gd_abandonedcart extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        include __DIR__ . '/version.php';
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'gd.abandonedcart';
        $this->MODULE_NAME = Loc::getMessage('CART_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('CART_MODULE_DESCRIPTION');

        $this->MODULE_GROUP_RIGHTS = 'N';

        $this->PARTNER_NAME = Loc::getMessage('CART_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('CART_PARTNER_URI');
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installFiles();
        \CAgent::AddAgent("\\GD\\LostCart\\SendPulse::sendData();", "gd.abandonedcart", "N", 2 * 3600, "", "Y");
        $this->installDB();
    }

    function doUninstall()
    {
        CAgent::RemoveAgent("\\GD\\LostCart\\SendPulse::sendData();", "gd.abandonedcart");
        $this->uninstallFiles();
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            LostCartTable::getEntity()->createDbTable();
        }
    }

    function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            if (Application::getConnection()->isTableExists(Base::getInstance('\GD\LostCart\LostCartTable')->getDBTableName())) {
                $connection = Application::getInstance()->getConnection();
                $connection->dropTable(LostCartTable::getTableName());
            }
        }
    }

    function installEvents()
    {
        return true;
    }

    function unInstallEvents()
    {
        return true;
    }

    function installFiles()
    {
        copyDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/admin/',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin',
            true,
            true
        );
        return true;
    }

    function uninstallFiles()
    {
        deleteDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/admin/',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin'
        );
        return true;
    }
}
