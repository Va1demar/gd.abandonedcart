<?php

namespace GD\LostCart;

use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Entity\DataManager;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class LostCartTable extends DataManager
{

    /**
     * Возвращает имя таблицы
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'gd_lostcart';
    }

    /**
     * Возвращает массив с полями таблицы
     *
     * @return array
     */
    public static function getMap(): array
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ],
            'NAME_CLIENT' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_NAME_CLIENT_TITLE'),
                'default_value' => function () {
                    return Loc::getMessage('CART_TABLE_NAME_CLIENT_DEFAULT');
                },
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
                'required'  => false
            ],
            'SURNAME_CLIENT' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_SURNAME_CLIENT_TITLE'),
                'default_value' => '',
                'required'  => false
            ],
            'PHONE' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_PHONE_TITLE'),
                'default_value' => '',
                'required'  => true
            ],
            'EMAIL' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_EMAIL_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 100),
                    ];
                },
                'required'  => true
            ],
            'FUSER' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_FUSER_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 100),
                    ];
                },
                'required'  => true
            ],
            'SITE_ID' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_SITE_ID_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 3),
                    ];
                },
                'required'  => true
            ],
            'PRODUCTS' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_PRODUCTS_TITLE'),
                'required'  => false
            ],
            'CREATE_DATE' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('CART_TABLE_CREATE_DATE_TITLE'),
                'required'  => true
            ],
            'ANSWER' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('CART_TABLE_PRODUCTS_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
                'required'  => false
            ],
        ];
    }
}
