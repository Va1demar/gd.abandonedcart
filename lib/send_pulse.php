<?php

namespace GD\LostCart;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Sale;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Data\Cache;
use GD\LostCart\Handler;

class SendPulse
{
    /**
     * Функция для агента
     *
     * @return string
     */
    public static function sendData(): string
    {
        $arCarts = Handler::getInstance()->getCarts();
        if (!empty($arCarts)) {
            foreach ($arCarts as $cart) {
                $arProducts = self::getProductsForBasket($cart['FUSER'], $cart['SITE_ID']);
                $products = $arRes = $arIblocks = [];
                foreach ($arProducts as $iblockXml => $arIds) {
                    if (isset($arIblocks[$iblockXml])) {
                        $iblock = $arIblocks[$iblockXml];
                    } else {
                        $iblock = self::getIblockByXml($iblockXml);
                        if ($iblock > 0) {
                            $arIblocks[$iblockXml] = $iblock;
                        } else {
                            continue;
                        }
                    }
                    $products = array_merge($products, self::getProducts($iblock, $arIds));
                }
                $arRes = [
                    'name' => $cart['NAME_CLIENT'],
                    'email' => $cart['EMAIL'],
                    'phone' => $cart['PHONE'],
                    'products' => $products
                ];
                if (!empty($linkService = self::getLinkForService())) {
                    $answer = self::sendOnService($linkService, $arRes);
                } else {
                    $answer = Loc::getMessage('NOT_LINK_SERVICE');
                }
                Handler::getInstance()->writeAnswer($cart['ID'], $answer, $arProducts);
            }
        }
        return '\\GD\\LostCart\\SendPulse::sendData();';
    }

    /**
     * Отправка данных через curl
     *
     * @param  array $arSend
     * @return string
     */
    private function sendOnService(string $link, array $arSend): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arSend, '', '&'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    /**
     * Возвращает информацию о товарах
     *
     * @param  int $iblock
     * @param  array $arProducts
     * @return array
     */
    private function getProducts(int $iblock, array $arProducts): array
    {
        $arResult = [];
        if ($iblock > 0 && !empty($arProducts)) {
            $arSelect = ['ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'];
            $arFilter = [
                'IBLOCK_ID' => $iblock,
                'ID' => $arProducts,
                'ACTIVE' => 'Y',
                [
                    'LOGIC' => 'OR',
                    '!PREVIEW_PICTURE' => false,
                    '!DETAIL_PICTURE' => false,
                ],
            ];
            $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            while ($arRes = $res->Fetch()) {
                $arResult[] = [
                    "name" => $arRes['NAME'],
                    "url" => self::createLink($arRes['CODE']),
                    "img" => $arRes['PREVIEW_PICTURE'] ? self::createImgLink($arRes['PREVIEW_PICTURE']) : self::createImgLink($arRes['DETAIL_PICTURE']),
                ];
            }
        }
        return $arResult;
    }

    /**
     * Формирует ссылку на товар
     *
     * @param  string $code
     * @return string
     */
    private function createLink(string $code): string
    {
        return 'https://mosplitka.ru/product/' . $code . '/';
    }

    /**
     * Формирует ссылку на картинку
     *
     * @param  int $file
     * @return string
     */
    private function createImgLink(int $file): string
    {

        return 'https://mosplitka.ru' . \CFile::ResizeImageGet($file, array('width' => self::getSizeImg()['WIDTH'], 'height' => self::getSizeImg()['HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];
    }

    /**
     * Массив с размерами картинок
     *
     * @return array
     */
    private function getSizeImg(): array
    {
        return [
            'WIDTH' => 300,
            'HEIGHT' => 300,
        ];
    }

    /**
     * Ссылка на сервис для отправки
     *
     * @return string
     */
    private function getLinkForService(): string
    {
        if (!empty($link = Option::get('gd.abandonedcart', 'link_sendpulse'))) {
            return $link;
        }
        return '';
    }

    /**
     * Получает товары из корзины пользователя
     *
     * @param  string $fuser
     * @param  string $siteId
     * @return array
     */
    private function getProductsForBasket(string $fuser, string $siteId): array
    {
        if (!Loader::includeModule('sale')) {
            return [];
        }
        $arResult = [];
        $dbRes = Sale\Basket::getList([
            'select' => ['PRODUCT_ID', 'CATALOG_XML_ID'],
            'filter' => [
                '=FUSER_ID' => $fuser,
                '=ORDER_ID' => null,
                '=LID' => $siteId,
                '=CAN_BUY' => 'Y',
            ],
            'limit' => 5
        ]);
        while ($item = $dbRes->fetch()) {
            $arResult[$item['CATALOG_XML_ID']][] = $item['PRODUCT_ID'];
        }
        return $arResult;
    }

    /**
     * Получает id инфоблоков по xml_id
     *
     * @param  string $xmlId
     * @return int
     */
    private function getIblockByXml(string $xmlId): int
    {
        $cache = Cache::createInstance();
        $iblockID = 0;
        if ($cache->initCache(86400, $xmlId, '/abandoned_cart/')) {
            $iblockID = $cache->getVars()['IBLOCK_ID'];
        } elseif ($cache->startDataCache()) {
            $dbRes = IblockTable::getList([
                'select' => ['ID'],
                'filter' => [
                    '=XML_ID' => $xmlId,
                ]
            ]);
            if ($dbRes) {
                $item = $dbRes->fetch();
                $iblockID = $item['ID'];
            }
            $cache->endDataCache(['IBLOCK_ID' => $iblockID]);
        }
        return $iblockID;
    }
}
