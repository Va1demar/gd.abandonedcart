<?php

namespace GD\LostCart;

use  GD\LostCart\LostCartTable;
use  Bitrix\Main\Type\DateTime;

class Handler
{
    private static $instance = null;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Добавляет или обновляет забытую корзину для отслеживания
     *
     * @param  array $arData
     * @return array
     */
    public function addAbandonedCart(array $arData): array
    {
        $arData = $this->convertData($arData);
        $arErrors = $this->validations($arData);
        $arData["CREATE_DATE"] = new DateTime();
        if (($id = $this->findIdEntry($arData["FUSER"])) > 0) {
            $arErrors = array_merge($arErrors, $this->updateAbandonedCart($id, $arData));
        } else {
            $result = LostCartTable::add($arData);
            if (!$result->isSuccess() && empty($arErrors)) {
                $arErrors = array_merge($arErrors, $result->getErrorMessages());
            }
        }
        return $arErrors;
    }

    /**
     * Проверка полей на корректность и возвращение массива ошибок
     *
     * @param  array $arFields
     * @return array
     */
    private function validations(array $arFields): array
    {
        $arErrors = [];
        if (!empty($error = $this->checkEmail($arFields['EMAIL']))) {
            $arErrors[] = $error;
        }
        if (!empty($error = $this->checkFuser($arFields['FUSER']))) {
            $arErrors[] = $error;
        }
        /*if (!empty($error = $this->checkProducts($arFields['PRODUCTS']))) {
            $arErrors[] = $error;
        }*/
        if (!empty($error = $this->checkPhone($arFields['PHONE']))) {
            $arErrors[] = $error;
        }
        if (!empty($error = $this->checkSiteId($arFields['SITE_ID']))) {
            $arErrors[] = $error;
        }
        return $arErrors;
    }

    /**
     * Проверка email на корректность
     *
     * @param  string $value Значение
     * @return string
     */
    private function checkEmail(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return 'ERROR_EMAIL';
        }
        if (iconv_strlen($value) < 2 && iconv_strlen($value) > 100) {
            return 'ERROR_EMAIL';
        }
        if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            return 'ERROR_EMAIL';
        }
        return '';
    }

    /**
     * Проверка fuser на пустоту
     *
     * @param  string $value Значение
     * @return string
     */
    private function checkFuser(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return 'ERROR_FUSER';
        }
        if (preg_match('/[^A-Za-z0-9]/', $value)) {
            return 'ERROR_FUSER';
        }
        return '';
    }

    /**
     * Проверка даты на корректность
     *
     * @param  string $value
     * @return string
     */
    private function checkProducts(string $value): string
    {
        $value = trim($value);
        if (empty(json_decode($value, true))) {
            return 'ERROR_PRODUCTS';
        }
        return '';
    }

    /**
     * Проверка телефона на пустоту
     *
     * @param  string $value
     * @return string
     */
    private function checkPhone(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return 'ERROR_PHONE';
        }
        return '';
    }
    
    /**
     * Проверка siteId на пустоту
     *
     * @param  string $value
     * @return string
     */
    private function checkSiteId(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return 'ERROR_SITE_ID';
        }
        return '';
    }

    /**
     * Преобразование данных для записи
     *
     * @param  array $arData
     * @return array
     */
    private function convertData(array $arData): array
    {
        $arResult = [];
        $arResult['NAME_CLIENT'] = $arData['NAME_CLIENT'] ?: '';
        $arResult['SURNAME_CLIENT'] = $arData['SURNAME_CLIENT'] ?: '';
        $arResult['PHONE'] = $arData['PHONE'] ?: '';
        $arResult['EMAIL'] = $arData['EMAIL'] ?: '';
        $arResult['FUSER'] = $arData['FUSER'] ?: '';
        $arResult['SITE_ID'] = $arData['SITE_ID'] ?: '';
        $arResult['CREATE_DATE'] = new DateTime();
        return $arResult;
    }

    /**
     * Обновляет запись и возвращает массив ошибок
     *
     * @param  int $id
     * @param  array $arData
     * @return array
     */
    public function updateAbandonedCart(int $id, array $arData): array
    {
        $arErrors = [];
        $result = LostCartTable::update($id, $arData);
        if (!$result->isSuccess()) {
            $arErrors = array_merge($arErrors, $result->getErrorMessages());
        }
        return $arErrors;
    }

    /**
     * Удаляет корзину из таблицы
     *
     * @param  string $fuser fuserID пользователя
     * @return array
     */
    public function deleteAbandonedCart(string $fuser): array
    {
        $arErrors = [];
        if (empty($this->checkFuser($fuser))) {
            if (($id = $this->findIdEntry($fuser)) > 0) {
                $result = LostCartTable::delete($id);
                if (!$result->isSuccess()) {
                    $arErrors = array_merge($arErrors, $result->getErrorMessages());
                }
            } else {
                $arErrors[] = 'Не найдена запись';
            }
        } else {
            $arErrors[] = 'FuserID некорректен';
        }

        return $arErrors;
    }

    /**
     * Ищет id записи по значению Fuser
     *
     * @param  mixed $fuser
     * @return int
     */
    private function findIdEntry(string $fuser): int
    {
        $arRes = LostCartTable::getList([
            'order' => ['ID' => 'DESC'],
            'select' => ['ID'],
            'filter' => ['FUSER' => $fuser, 'ANSWER' => false],
            'limit' => 1,
        ])->fetch();
        if ($arRes) {
            return $arRes['ID'];
        } else {
            return 0;
        }
    }

    /**
     * Получить записи, которые нужно отправить в sendPulse
     *
     * @return array
     */
    public function getCarts(): array
    {
        $objDateTime = new DateTime();
        $objDateTime->add("-2 hour");
        $arRes = LostCartTable::getList([
            'order' => ['ID' => 'ASC'],
            'select' => ['ID', 'NAME_CLIENT', 'EMAIL', 'PHONE', 'FUSER', 'SITE_ID'],
            'filter' => ['<CREATE_DATE' => $objDateTime, 'ANSWER' => false],
            'limit' => 100,
        ])->fetchAll();
        return $arRes;
    }
    
    /**
     * Записать ответ сервера
     *
     * @param  int $id
     * @param  string $answer
     * @param  array $products
     * @return array
     */
    public function writeAnswer(int $id, string $answer, array $products): array
    {
        $arErrors = [];
        $arData["ANSWER"] = $answer;
        $arData["PRODUCTS"] = json_encode($products);
        $result = LostCartTable::update($id, $arData);
        if (!$result->isSuccess()) {
            $arErrors = $result->getErrorMessages();
        }
        return $arErrors;
    }
}
