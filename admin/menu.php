<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$aMenu = array(
    array(
        'parent_menu' => 'global_menu_marketing',
        'sort' => 2000,
        'text' => Loc::getMessage('CART_TITLE'),
        'title' => Loc::getMessage('CART_TEXT'),
        'url' => 'abandoned_cart_index.php?lang=' . LANGUAGE_ID,
        'icon' => 'sale_menu_icon_marketplace',
        'page_icon' => 'sale_menu_icon_marketplace',
        'items_id' => 'menu_references'
    )
);
return $aMenu;
