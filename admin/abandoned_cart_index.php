<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use GD\LostCart\LostCartTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

if (!Loader::includeModule('gd.abandonedcart')) return false;

$POST_RIGHT = $APPLICATION->GetGroupRight("gd_abandonedcart");

if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$sTableID = "gd_lostcart";
$oSort = new \CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new \CAdminList($sTableID, $oSort);

$rsData = LostCartTable::getList(['order' => ['ID' => 'DESC']]);

$rsData = new \CAdminResult($rsData, $sTableID);

$rsData->NavStart();

$lAdmin->NavText($rsData->GetNavPrint(Loc::getMessage("NAV")));

$lAdmin->AddHeaders(array(
    array(
        "id"    => "ID",
        "content"  => "ID",
        "sort"    => "id",
        "align"    => "right",
        "default"  => true,
    ),
    array(
        "id"    => "NAME_CLIENT",
        "content"  => Loc::getMessage("NAME_CLIENT"),
        "sort"    => "name_client",
        "default"  => true,
    ),
    array(
        "id"    => "SURNAME_CLIENT",
        "content"  => Loc::getMessage("SURNAME_CLIENT"),
        "sort"    => "surname_client",
        "default"  => true,
    ),
    array(
        "id"    => "PHONE",
        "content"  => Loc::getMessage("PHONE"),
        "sort"    => "phone",
        "default"  => true,
    ),
    array(
        "id"    => "EMAIL",
        "content"  => Loc::getMessage("EMAIL"),
        "sort"    => "email",
        "default"  => true,
    ),
    array(
        "id"    => "FUSER",
        "content"  => Loc::getMessage("FUSER"),
        "sort"    => "fuser",
        "default"  => true,
    ),
    array(
        "id"    => "SITE_ID",
        "content"  => Loc::getMessage("SITE_ID"),
        "sort"    => "siteid",
        "default"  => true,
    ),
    array(
        "id"    => "PRODUCTS",
        "content"  => Loc::getMessage("PRODUCTS"),
        "sort"    => "products",
        "default"  => true,
    ),
    array(
        "id"    => "CREATE_DATE",
        "content"  => Loc::getMessage("CREATE_DATE"),
        "sort"    => "create_date",
        "default"  => true,
    ),
    array(
        "id"    => "ANSWER",
        "content"  => Loc::getMessage("ANSWER"),
        "sort"    => "answer",
        "default"  => true,
    ),
));

while ($arRes = $rsData->NavNext(true, "f_")){
    $lAdmin->AddRow($f_ID, $arRes);
}

$APPLICATION->SetTitle(Loc::getMessage("TITLE"));

$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
