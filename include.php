<?php
Bitrix\Main\Loader::registerAutoloadClasses(
    "gd.abandonedcart",
    [
        "GD\\LostCart\\LostCartTable" => "lib/table.php",
        "GD\\LostCart\\Handler" => "lib/handler.php",
        "GD\\LostCart\\SendPulse" => "lib/send_pulse.php",
    ]
);
