<?php
$MESS['CART_OPTIONS_TAB_1'] = 'Настройки';
$MESS['CART_OPTIONS_TAB_1_TITLE'] = 'Настройки';
$MESS['CART_OPTIONS_SENDPULSE'] = 'Sendpulse';
$MESS['CART_OPTIONS_SENDPULSE_LINK'] = 'Ссылка для отправки корзины';
$MESS['CART_OPTIONS_INPUT_APPLY'] = 'Применить';
$MESS['CART_OPTIONS_INPUT_DEFAULT'] = 'Установить по умолчанию';
