<?php
$MESS['NAME_CLIENT'] = 'Имя';
$MESS['SURNAME_CLIENT'] = 'Фамилия';
$MESS['PHONE'] = 'Телефон';
$MESS['EMAIL'] = 'Email';
$MESS['FUSER'] = 'FUser';
$MESS['SITE_ID'] = 'Site ID';
$MESS['PRODUCTS'] = 'Товары корзины';
$MESS['CREATE_DATE'] = 'Дата создания';
$MESS['ANSWER'] = 'Ответ сервиса';

$MESS['ACCESS_DENIED'] = 'Нет доступа к модулю';
$MESS['TITLE'] = 'Брошенная корзина';
$MESS['NAV'] = 'Страница';